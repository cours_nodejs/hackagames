// Import module
import {Pool, PoolClient} from 'pg';

// Import config for database connection
import postgresql from '../config/postgresql';

const pool = new Pool(postgresql);

let client: PoolClient;

/**
 * Get a single row of data
 * @param {string} request Query
 * @param {String[]} parameters List of parameters
 */
export async function getRow(request: string, parameters: any[]) {
  const {rows} = await pool.query(request, parameters);
  return rows[0];
};

/**
 * Get all rows
 * @param {string} request Query
 * @param {String[]} parameters List of parameters
 */
export async function getRows(request: string, parameters: any[]) {
  const {rows} = await pool.query(request, parameters);
  return rows;
}

/**
 * Execute a sql request (UPDATE/CREATE/DELETE)
 * @param {string} request Query
 * @param {String[]} parameters List of parameters
 */
export async function execSql(request: string, parameters: any[]) {
  const {rows} = await pool.query(request, parameters);
  return rows[0];
}

/**
 * Start a transaction
 */
export async function startTransaction() {
  client = await pool.connect();
  await client.query('BEGIN');
  return;
}

/**
 * Commit a transaction
 */
export async function commit() {
  await client.query('COMMIT');
  client.release();
  return;
}

/**
 * Rollback a transaction
 */
export async function rollback() {
  await client.query('ROLLBACK');
  client.release();
  return;
}
