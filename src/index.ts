// Import node modules
import express from 'express';
import helmet from 'helmet';
import cors from 'cors';

// Create express app
const app = express();

// Register modules
app.use(cors());
app.use(helmet());
app.use(express.json());

// Import controller dealing with error
import {errorHandler} from './controllers/error.controller';
import {gameController} from './controllers/game.controller';

// Expose entry points
app.use('/game', gameController);
app.use(errorHandler);

// Start server
app.listen(3000, () => {
  console.log('SERVER RUNNING ON PORT 3000');
});

// Close server on signals SIGINT/SIGTERM
process.on('SIGINT', () => {
  console.log('SIGINT signal received');
  process.exit();
});

process.on('SIGTERM', () => {
  console.log('SIGTERM signal received');
  process.exit();
});
