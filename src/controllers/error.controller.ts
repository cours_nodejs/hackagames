// Import express types
import {Request, Response, NextFunction} from 'express';
// Import interface for HttpError
import {HttpError} from '../interfaces/error.interface';

/**
 * Send error code and message for errors
 * @param {HttpError} err Error raised
 * @param {Request} _req Request received
 * @param {Response} res Response to send
 * @param {NextFunction} _next Next route to call
 */
export const errorHandler = (err: HttpError, _req: Request, res: Response, _next: NextFunction) => {
  res.status(err.status).send(err.message);
};
