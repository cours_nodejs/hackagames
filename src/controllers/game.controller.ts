import express, {Request, Response, NextFunction} from 'express';
import GameService from '../services/game.service';

const gameService: GameService = new GameService();

export const gameController = express.Router();

gameController.post('/start', (req: Request, res: Response, next: NextFunction) => {
  try {
    gameService.startGame();

    res.status(200).send('Server started');
  } catch (error: any) {
    // Transit error to error controller
    next({
      status: error.status || 500,
      message: error.message,
    });
  }
});

gameController.post('/stop', async (req: Request, res: Response, next: NextFunction) => {
  try {
    await gameService.stopGame();

    res.status(200).send('Server stopped');
  } catch (error: any) {
    // Transit error to error controller
    next({
      status: error.status || 500,
      message: error.message,
    });
  }
});

gameController.post('/add-ia', (req: Request, res: Response, next: NextFunction) => {
  try {
    gameService.addIAToTheGame();

    res.status(200).send('IA Added');
  } catch (error: any) {
    // Transit error to error controller
    next({
      status: error.status || 500,
      message: error.message,
    });
  }
});

gameController.post('/join', async (req: Request, res: Response, next: NextFunction) => {
  try {
    await gameService.joinGame();

    res.status(200).send('Player joined');
  } catch (error: any) {
    // Transit error to error controller
    next({
      status: error.status || 500,
      message: error.message,
    });
  }
});

gameController.post('/play', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const {command} = req.body;
    await gameService.sendCommand(command);

    res.status(200).send('Move played');
  } catch (error: any) {
    // Transit error to error controller
    next({
      status: error.status || 500,
      message: error.message,
    });
  }
});
