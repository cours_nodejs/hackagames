import {ChildProcess, spawn} from 'child_process';
import {Telnet} from 'telnet-client';

const baseFolder = '../hackagames/game-risky';

/**
 * Service class handling the game logic
 */
export default class GameService {
  game_process: ChildProcess;
  ia_processes: ChildProcess[];
  connection: Telnet;

  /**
   * Constructor
   */
  constructor() {
    this.game_process = null;
    this.ia_processes = [];
    this.connection = new Telnet();
  }

  /**
   * Start the server
   */
  startGame(): void {
    this.game_process = spawn(`${baseFolder}/hg-risky`, []);

    console.log('Game started');

    this.game_process.stderr.on('data', (data) => {
      console.log(data.toString());
    });

    this.game_process.stdout.on('data', (data) => {
      console.log(data.toString());
    });
  }

  /**
   * Stop the server
   */
  async stopGame(): Promise<void> {
    for (const iaProcess of this.ia_processes) {
      iaProcess.kill('SIGINT');
    }
    await this.connection.destroy();
    this.game_process.kill('SIGINT');
  }

  /**
   * Add an IA to the game
   */
  addIAToTheGame(): void {
    const newIa: ChildProcess = spawn('python3', [`${baseFolder}/simplePlayer.py`]);

    this.ia_processes.push(newIa);

    console.log('IA added');

    newIa.stderr.on('data', (data) => {
      console.log(data.toString());
    });

    newIa.stdout.on('data', (data) => {
      console.log(data.toString());
    });
  }

  /**
   * Join the game as a player
   */
  async joinGame(): Promise<void> {
    const params = {
      host: 'localhost',
      port: 14001,
      negotiationMandatory: false,
      timeout: 1500,
    };

    await this.connection.connect(params);

    const socket = this.connection.getSocket();

    socket.on('data', (data) => {
      console.log(data.toString());
    });

    socket.on('error', (data) => {
      console.log(data.toString());
    });

    console.log('Player joined');
  }

  /**
   * Send an event to the connection
   * @param {String} command Command to send
   */
  async sendCommand(command: string): Promise<void> {
    await this.connection.write(command);

    console.log('Command sent');
  }
}

